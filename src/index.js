import React from 'react';
import ReactDOM from 'react-dom';

export default class Progress extends React.Component {

	constructor(props) {
    super(props);
    this.state = {
			value: 1
		};
	}

	handleClick(event) {
  	var percentage = ((event.pageX - event.target.offsetLeft) / event.target.offsetWidth) * 100;
    this.setState({
			value: percentage
		});
  }

	render() {
  	return <progress onClick={(event) => this.handleClick(event)} value={this.state.value} max="100">{this.state.value}</progress>
  }
	
}

ReactDOM.render(
  <Progress />,
  document.getElementById('progress')
);
